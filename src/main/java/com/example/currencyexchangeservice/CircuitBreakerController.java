package com.example.currencyexchangeservice;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.ast.Test;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@RestController
@Slf4j
public class CircuitBreakerController {


    @GetMapping("/sample-api")
//    @Retry(name = "sample-api", fallbackMethod = "response")
    @CircuitBreaker(name = "default", fallbackMethod = "response")
    public String sample(){
        log.info("calling sample api");
        return Objects.requireNonNull(new RestTemplate().getForObject("dummy", Test.class)).toString();
    }


    public String response(Exception e){
        return "services temporarily unavailable";
    }
}
